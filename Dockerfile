ARG DOCKER_VERSION=stable

FROM docker:${DOCKER_VERSION}
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Add coreutils for version sort and jq to extract data from Docker JSON outputs.
RUN apk add --no-cache ca-certificates coreutils jq